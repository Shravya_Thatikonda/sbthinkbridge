## Shop Bridge

One Page Application
It represents a digital inventory of items of Vac's Bakery. 
Cateory of Items include: Cakes, Desserts, Pizza and Sides & Beverages
The Admin, can create, update, delete items in the stock.
---

## Test cases,  Project Breakdown, Project UI screens

Link to google sheet containing the project breakdown, project UI screens and test cases:
https://docs.google.com/spreadsheets/d/1DvCyRfTockJCUsRGlDD9wGxYcGbnxARjRn6ZeRjcevw/edit?usp=sharing

---

## Clone a repository from master branch

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

## Run npm install and ng serve to get started

#Plugins Used
NGPrime components and icons are used to achieve the functionality